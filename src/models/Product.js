const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {
    
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    description:{
        type: DataTypes.TEXT,
        allowNull:false
    },
    amount: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    evaluation: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    paymentMethod: {
        type: DataTypes.STRING,
        allowNull: false
    },
    photo:{
        type: DataTypes.TEXT,
        allowNull:false
    }
});

Product.associate = function(models) {
    Product.belongsTo(models.User);
    Product.belongsTo(models.Store);
};


module.exports = Product;